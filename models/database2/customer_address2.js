'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Customer_Address2 extends Model {

    static associate(models) {
      // Customer_Address2.belongsTo(models.database1.models.Customer, {
      //   foreignKey: 'customer_id',
      //   as: 'customer'
      // });

    }
  };
  Customer_Address2.init({
    address_house: DataTypes.STRING,
    address_street: DataTypes.STRING,
    address_city: DataTypes.STRING,
    customer_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Customer',
        key: 'customer_id'
      },
    },
  }, {
    sequelize,
    modelName: 'Customer_Address2',
  });

  // Customer_Address2.belongsTo(Customer_Address2, { as: 'customer', through: 'Customer', foreignKey: 'customer_id'});
  return Customer_Address2;
};