'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];

const db = {};
const databases = Object.keys(config.databases);

for (let i = 0; i < databases.length; ++i) {
    let database = databases[i];
    let dbPath = config.databases[database];
    db[database] = new Sequelize(dbPath.database, dbPath.username, dbPath.password, dbPath);
}

fs
    .readdirSync(__dirname + '/database1')
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
    })
    .forEach(file => {
        const model = require(path.join(__dirname + '/database1', file))(db.database1, Sequelize.DataTypes)
        db[model.name] = model;
    });

fs
    .readdirSync(__dirname + '/database2')
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
    })
    .forEach(file => {
        const model = require(path.join(__dirname + '/database2', file))(db.database2, Sequelize.DataTypes)
        db[model.name] = model;
    });

fs
    .readdirSync(__dirname + '/database3')
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
    })
    .forEach(file => {
        const model = require(path.join(__dirname + '/database3', file))(db.database3, Sequelize.DataTypes)
        db[model.name] = model;
    });

    // db.comments.belongsTo(db.posts);

// db.database1.models.Customer.belongsTo(db.database2.models.Customer_Address2)
// db.Customer.belongsTo(db.Customer_Address3)
// db.database2.models.Customer_Address2.hasOne(db.database1.models.Customer)
// db.Customer_Address3.hasOne(db.Customer)


module.exports = db;
