'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Customer extends Model {
   
    static associate(models) {
      // Customer.hasOne(models.database2.models.Customer_Address2, {
      //   foreignKey: 'customer_id',
      //   as: 'customer_address2'
      // });
      // Customer.hasOne(models.database3.models.Customer_Address3, {
      //   foreignKey: 'customer_id',
      //   as: 'customer_address3'
      // });
    }
  };
  Customer.init({
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    email: DataTypes.STRING,
    mobile: DataTypes.STRING,
    password: DataTypes.STRING,
    database_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Customer',
  });
  //Customer.hasOne(Customer, { as: 'customer_address2', through: 'Customer_Address2', foreignKey: 'customer_id'});

  return Customer;
};