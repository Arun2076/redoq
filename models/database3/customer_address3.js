'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Customer_Address3 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Customer_Address3.belongsTo(models.database1.models.Customer, {
      //   foreignKey: 'customer_id',
      //   as: 'customer'
      // });
    }
  };
  
  Customer_Address3.init({
    address_house: DataTypes.STRING,
    address_street: DataTypes.STRING,
    address_city: DataTypes.STRING,
    customer_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Customer',
        key: 'customer_id'
      },
    },
  }, {
    sequelize,
    modelName: 'Customer_Address3',
  });
  return Customer_Address3;
};