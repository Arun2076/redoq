const jwt = require('jsonwebtoken')
const MODULE_CREATE_SECRET = process.env.MODULE_CREATE_SECRET
const ModuleModal = require('../models/module').modal
exports.generateModuleToken = async (data ) => {
    try{
        let token = await jwt.sign(data, MODULE_CREATE_SECRET, {})
        return token
    }catch (err){
        console.log(err.message)
    }
}

exports.matchModuleToken = async (data) => {
    try {
        const result = await jwt.verify(data, MODULE_CREATE_SECRET)
        if (result.name) {
            let check = await ModuleModal.findOne({ name: result.name, moduleToken: data })
            if(check) return true

            return false
        } else {
            return false
        }
    } catch (error) {
        return false
    }
}