var express = require('express')
var router = express.Router()

const customer_address2_controller = require('../controller/index').customer_address2

/* Classroom Router */
router.post('/', customer_address2_controller.create)

module.exports = router