var express = require('express')
var router = express.Router()

const customer_address1_controller = require('../controller/index').customer_address1

/* Classroom Router */
router.post('/', customer_address1_controller.create)

module.exports = router