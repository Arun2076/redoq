var express = require('express')
var router = express.Router()

const customerController = require('../controller/index').customer

/* Classroom Router */
router.post('/login',customerController.login)
router.post('/:id',customerController.readOneById)
router.post('/', customerController.create)


module.exports = router