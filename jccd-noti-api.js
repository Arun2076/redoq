require('dotenv').config({
      path: `${__dirname}/.env.${process.env.NODE_ENV}`,
})

const env = process.env.NODE_ENV

const express = require('express')
const app = express()
const port = process.env.PORT
const bodyParser = require('body-parser')
const formData = require('express-form-data')
const cors = require('cors')
app.use(bodyParser.json({ limit: '10mb' }))
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))
app.use(cors())
app.use(formData.format())

const server = app.listen(port, () =>
      console.log(`running on ${env} listing to the port. ${port}`)
)

const routers = require('./routes/index')

app.use('/api/customer/address1', routers.customer)
app.use('/api/customer/address2', routers.customer)
app.use('/api/customer', routers.customer)

module.exports = server