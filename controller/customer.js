const Customer = require("../models").database1.models.Customer
const Customer_Address2 = require("../models").database2.models.Customer_Address2
const Customer_Address3 = require("../models").database3.models.Customer_Address3

exports.login = (req, res) => {

    let where = { mobile: req.body.mobile, password: req.body.password }

    Customer.findOne(where)
        .then(data => {
            res.status(200).send(data)
        })
        .catch(error => {
            res.status(500).send({
                message:
                    error.message || "Some error occurred while creating the Customer."
            })
        })
}

// Create and Save a new Tutorial
exports.create = (req, res) => {

    Customer.create(req.body)
        .then(data => {
            res.status(201).send(data)
        })
        .catch(error => {
            res.status(500).send({
                message:
                    error.message || "Some error occurred while creating the Customer."
            })
        })
}

exports.readAll = (req, res) => {
    Customer
        .findAll()
        .then((data) => res.status(200).send(data))
        .catch((error) => {
            res.status(500).send({
                message:
                    error.message || "Some error occurred while reading the Customer."
            })
        })
}

exports.readOneById = (req, res) => {
    Customer
        .findByPk(req.params.id)
        .then((data) => {
            if (data.database_id == 2) {
                Customer_Address2
                    .findOne({ customer_id: data.id })
                    .then((address) => res.status(200).send(address))
                    .catch((error) => {
                        res.status(500).send({
                            message:
                                error.message || "Some error occurred while reading the Customer."
                        })
                    })
            } else if (data.database_id == 3) {
                Customer_Address3
                    .findOne({ customer_id: data.id })
                    .then((address) => res.status(200).send(address))
                    .catch((error) => {
                        res.status(500).send({
                            message:
                                error.message || "Some error occurred while reading the Customer."
                        })
                    })
            } else {
                res.status(200).send(data)
            }
        })
        .catch((error) => {
            console.log(error)
            res.status(500).send({
                message:
                    error.message || "Some error occurred while reading the Customer."
            })
        })
}