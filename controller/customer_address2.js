const Customer_Address2 = require("../models").Customer_Address2

// Customer_Address1: Customer_Address1,
//   Customer_Address2: Customer_Address2
exports.create = (req, res) => {

    Customer_Address2.create(req.body)
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Tutorial."
            })
        })
}