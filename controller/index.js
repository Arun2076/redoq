const customer = require('./customer');
const customer_address1 = require('./customer_address1');
const customer_address2 = require('./customer_address2');

module.exports = {
  customer,
  customer_address1,
  customer_address2
};